package com.soaint.fogacoop.rules.service.implementador;

import com.soaint.fogacoop.rules.commons.domains.request.PersonaDTORequest;
import com.soaint.fogacoop.rules.model.entities.Persona;


import java.util.Collection;
import java.util.Optional;

public interface IGestionPersona {

    Optional<Persona> registerPersona(final PersonaDTORequest persona);

    Optional<Collection<Persona>> findPersonas();

    Optional<Persona> getPersonaById(String id);

}
