package com.soaint.fogacoop.rules.service.implementador.impl;

import com.soaint.fogacoop.rules.commons.domains.request.PersonaDTORequest;
import com.soaint.fogacoop.rules.model.entities.Persona;
import com.soaint.fogacoop.rules.repository.persona.impl.PersonaRepositoryFacade;
import com.soaint.fogacoop.rules.service.implementador.IGestionPersona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class GestionPersonaImpl implements IGestionPersona {

    private final PersonaRepositoryFacade repository;

    @Autowired
    public GestionPersonaImpl(PersonaRepositoryFacade repository) {
        this.repository = repository;
    }

    @Override
    public Optional<Persona> registerPersona(PersonaDTORequest persona) {
        return repository.registerPersona(persona);
    }

    @Override
    public Optional<Collection<Persona>> findPersonas() {
        return repository.findPersonas();
    }

    @Override
    public Optional<Persona> getPersonaById(String id) {
        return repository.getPersonaById(id);
    }
}
