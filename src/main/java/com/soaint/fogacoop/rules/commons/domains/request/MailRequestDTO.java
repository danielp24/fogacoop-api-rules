package com.soaint.fogacoop.rules.commons.domains.request;

import lombok.*;

import java.util.Map;


@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@ToString
public class MailRequestDTO {

    private String[] to;
    private String [] cc;
    private Map<String,String> parameters;
    private String subject;
    private int template;
    private byte[] attachments;
    private String nameAttachments;
    private String contentTypeattachment;

}
