package com.soaint.fogacoop.rules.commons.domains.request;


import com.soaint.fogacoop.rules.commons.domains.generic.ImplementadorDTO;

import java.util.HashSet;

public class ImplementadorDTORequest extends ImplementadorDTO {

    private Double salario;
    private HashSet<String> actividades;

}
