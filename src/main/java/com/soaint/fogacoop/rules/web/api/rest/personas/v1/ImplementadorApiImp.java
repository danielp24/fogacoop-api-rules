package com.soaint.fogacoop.rules.web.api.rest.personas.v1;

import com.soaint.fogacoop.rules.commons.constants.api.persona.EndpointPersonaApi;
import com.soaint.fogacoop.rules.commons.domains.request.PersonaDTORequest;
import com.soaint.fogacoop.rules.commons.domains.response.builder.ResponseBuilder;
import com.soaint.fogacoop.rules.model.entities.Persona;
import com.soaint.fogacoop.rules.service.implementador.IGestionPersona;
import com.soaint.fogacoop.rules.web.api.rest.ImplementadorApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = EndpointPersonaApi.PERSONA_API_V1)
public class ImplementadorApiImp implements ImplementadorApi {

    private final IGestionPersona gestionPersona;

    @Autowired
    public ImplementadorApiImp(IGestionPersona gestionPersona) {
        this.gestionPersona = gestionPersona;
    }

    @GetMapping(EndpointPersonaApi.FIND_PERSONAS)
    public ResponseEntity findPersonas(){
        return ResponseBuilder.newBuilder()
                .withStatus(HttpStatus.OK)
                .withResponse(gestionPersona.findPersonas())
                .buildResponse();
    }


    public ResponseEntity findPersonById(final String id){



        return  ResponseBuilder.newBuilder()
                .withStatus(HttpStatus.OK)
                .withResponse(gestionPersona.findPersonas())
                .buildResponse();
    }

    @PutMapping(EndpointPersonaApi.UPDATE_PERSONAS_BY_ID)
    public ResponseEntity updatePersonaById(final String id){
        return null;
    }

    @PostMapping(EndpointPersonaApi.CREATE_PERSONA)
    public ResponseEntity createPersona(final PersonaDTORequest persona){
        Optional<Persona> personaCreated = gestionPersona.registerPersona(persona);
        return
             ResponseBuilder.newBuilder()
                    .withStatus(personaCreated.isPresent() ? HttpStatus.CREATED: HttpStatus.OK)
                    .withResponse(personaCreated.isPresent() ? personaCreated : new Persona())
                    .buildResponse() ;
    }

}
