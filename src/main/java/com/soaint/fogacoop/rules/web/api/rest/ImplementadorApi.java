package com.soaint.fogacoop.rules.web.api.rest;

import com.soaint.fogacoop.rules.commons.domains.request.PersonaDTORequest;
import org.springframework.http.ResponseEntity;

public interface ImplementadorApi {

    ResponseEntity<?> findPersonas();

    ResponseEntity<?> findPersonById(final String id);

    ResponseEntity<?> updatePersonaById(final String id);

    ResponseEntity<?> createPersona(final PersonaDTORequest persona);

}
